'use strict';

angular.module('bs-angular.task2', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/task2', {
            templateUrl: 'task2/task2.html',
            controller: 'Task2Controller'
        });
    }])

    .controller('Task2Controller', ['$scope', function($scope) {
        $scope.ibanNumber = "";
    }])

    .directive('validateIban', function() {
        return {
            restrict: 'E',
            templateUrl: 'task2/validate-iban.html',
            link: function($scope) {

                const ibanCountries = [
                    { country: 'Poland', symbol: 'PL', lengthNumber: 28 },
                    { country: 'Ireland', symbol: 'IE', lengthNumber: 22 },
                    { country: 'Spain', symbol: 'ES', lengthNumber: 24 },
                    { country: 'Italy', symbol: 'IT', lengthNumber: 27 },
                    { country: 'Iceland', symbol: 'IS', lengthNumber: 26 }
                ];

                const alphabet = {
                    "A": 10, "B": 11, "C": 12, "D": 13, "E": 14, "F": 15, "G": 16, "H": 17, "I": 18, "J": 19, "K": 20, "L": 21, "M": 22,
                    "N": 23, "O": 24, "P": 25, "Q": 26, "R": 27, "S": 28, "T": 29, "U": 30, "V": 31, "W": 32, "X": 33, "Y": 34, "Z": 35
                };

                $scope.validateIban = function() {
                    $scope.alert = '';
                    $scope.alertStatus = '';

                    let ibanNumber = $scope.ibanNumber.replace(/ /g, "");
                    let symbol = ibanNumber.substring(0, 2);
                    let lengthNumber = 0;

                    for(let i = 0; i < ibanCountries.length; i++) {
                        if (ibanCountries[i].symbol === symbol) {
                            lengthNumber = ibanCountries[i].lengthNumber;
                            break;
                        }
                    }

                    if(lengthNumber === 0) {
                        $scope.alert = "Brak danych walidacyjnych dla podanego kraju.";
                        $scope.alertStatus = "Info";
                    } else if(ibanNumber.length === lengthNumber) {
                        let result = ibanNumber.substr(4) + ibanNumber.substr(0, 4);

                        for (let i = 0; i < result.length; i++) {
                            if (alphabet.hasOwnProperty( result[i] )) {
                                result = result.replace(result[i], alphabet[result[i]]);
                            }
                        }

                        result = parseInt(result, 10) % 97;

                        if(result === 1) {
                            $scope.alert = "Numer IBAN " + $scope.ibanNumber + " jest prawidłowy.";
                            $scope.alertStatus = "Success";
                        } else {
                            $scope.alert = "Numer IBAN " + $scope.ibanNumber + " jest nieprawidłowy.";
                            $scope.alertStatus = "Incorrect";
                        }
                    } else {
                        $scope.alert = "Błędna długość znaków dla podanego kraju.";
                        $scope.alertStatus = "Incorrect";
                    }
                }
            }
        };
    });
