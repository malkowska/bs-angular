'use strict';

angular.module('bs-angular.task3', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/task3', {
            templateUrl: 'task3/task3.html',
            controller: 'Task3Controller'
        });
    }])

    .controller('Task3Controller', ['$scope', '$resource', 'NgTableParams', '$http', function($scope, $resource, NgTableParams, $http) {

        let vm = this;

        vm.tableParams = new NgTableParams({}, {
            getData: function (params) {
                return $http({
                    method: 'GET',
                    data: '',
                    url: 'http://young-water-8859.getsandbox.com/transfers',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function(response) {
                    if (response.status === 200) {
                        return response.data;
                    }
                });
            }
        });
    }]);
