'use strict';

angular.module('bs-angular', [
    'ngRoute',
    'ngResource',
    'ngTable',
    'bs-angular.task1',
    'bs-angular.task2',
    'bs-angular.task3'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider.otherwise({redirectTo: '/task1'});
}]);
