'use strict';

angular.module('bs-angular.task1', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/task1', {
            templateUrl: 'task1/task1.html',
            controller: 'Task1Controller'
        });
    }])

    .controller('Task1Controller', ['$scope', '$q', '$location', function($scope, $q, $location) {
        $scope.isActive = function(route) {
            return route === $location.path();
        };
        $scope.taxSymbols = [
            { value: { symbol: 'CIT' } },
            { value: { symbol: 'PIT' } },
            { value: { symbol: 'PIT24' } },
            { value: { symbol: 'VAT' } },
            { value: { symbol: 'VAT-7' } },
        ];
        $scope.userInput = '';

        function searchTaxSymbols(phrase) {
            let deferred = $q.defer(),
                symbols = (function () {
                    let output = [],
                        re = new RegExp(phrase.replace(/[&\/\\#,+()\[\]$~%.'":*?<>{}]/gi, ''), 'i'),
                        o = [],
                        symbol = '';

                    if ($scope.taxSymbols) {
                        angular.forEach($scope.taxSymbols, function (v) {
                            symbol = v.value.symbol;
                            if (re.test(symbol)) {
                                o = {
                                    value: symbol
                                };
                                output.push(o);
                            }
                        });
                    }

                    return output;
                })();

            deferred.resolve(symbols);

            return deferred.promise;
        }

        $scope.$watch('userInput', function (newValue, oldValue) {
            searchTaxSymbols(newValue).then(function (symbols) {
                $scope.symbols = symbols;
            });
        });
    }]);
